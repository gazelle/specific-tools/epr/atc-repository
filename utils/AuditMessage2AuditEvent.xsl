<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output omit-xml-declaration="yes" indent="yes"/>


    <xsl:template match="AuditMessage">
        <xsl:element namespace="http://hl7.org/fhir" name="f:AuditEvent">
            <xsl:apply-templates select="EventIdentification"/>
            <!-- looping through ActiveParticipant to create agent -->
            <xsl:for-each select="ActiveParticipant">
                <xsl:apply-templates select="current()"/>
            </xsl:for-each>
            <!-- source -->
            <xsl:apply-templates select="AuditSourceIdentification"/>
            <!-- looping through ParticipantObjectIdentification to create entity -->
            <xsl:for-each select="ParticipantObjectIdentification">
                <xsl:apply-templates select="current()"/>
            </xsl:for-each>
        </xsl:element>
    </xsl:template>

    <xsl:template match="EventIdentification">
        <!-- extract the information from the EventIdentification part of the ATNA message -->
        <!--type-->
        <xsl:apply-templates select="EventID"/>
        <!--subtype-->
        <xsl:apply-templates select="EventTypeCode"/>
        <!--action-->
        <xsl:element namespace="http://hl7.org/fhir" name="f:action">
            <xsl:call-template name="Code">
                <xsl:with-param name="value" select="@EventActionCode"/>
            </xsl:call-template>
        </xsl:element>
        <!--recorded-->
        <xsl:element namespace="http://hl7.org/fhir" name="f:recorded">
            <xsl:call-template name="Instant">
                <xsl:with-param name="value" select="@EventDateTime"/>
            </xsl:call-template>
        </xsl:element>
        <!--outcome-->
        <xsl:element namespace="http://hl7.org/fhir" name="f:outcome">
            <xsl:call-template name="Code">
                <xsl:with-param name="value" select="@EventOutcomeIndicator"/>
            </xsl:call-template>
        </xsl:element>
        <!-- outcomeDesc-->
        <xsl:if test="@EventOutcomeIndicator">
            <xsl:element namespace="http://hl7.org/fhir" name="f:outcomeDesc">
                <xsl:call-template name="String">
                    <xsl:with-param name="value" select="@EventOutcomeIndicator"/>
                </xsl:call-template>
            </xsl:element>
        </xsl:if>
        <!-- purposeOfEvent (cannot find EventPurposeOfUse in DICOM schema of the audit message)-->
    </xsl:template>

    <xsl:template match="EventID">
        <!-- convert AuditMessage/EventIdentification/EventID into AuditEvent/type -->
        <xsl:element namespace="http://hl7.org/fhir" name="f:type">
            <xsl:call-template name="Coding">
                <xsl:with-param name="csdcode" select="@csd-code"/>
                <xsl:with-param name="codeSystemName" select="@codeSystemName"/>
                <xsl:with-param name="originalText" select="@originalText"/>
            </xsl:call-template>
        </xsl:element>
    </xsl:template>

    <xsl:template match="EventTypeCode">
        <!-- convert AuditMessage/EventIdentification/EventTypeCode into AuditEvent/subtype -->
        <xsl:element namespace="http://hl7.org/fhir" name="f:subtype">
            <xsl:call-template name="Coding">
                <xsl:with-param name="csdcode" select="@csd-code"/>
                <xsl:with-param name="codeSystemName" select="@codeSystemName"/>
                <xsl:with-param name="originalText" select="@originalText"/>
            </xsl:call-template>
        </xsl:element>
    </xsl:template>

    <xsl:template match="ActiveParticipant">
        <xsl:element namespace="http://hl7.org/fhir" name="f:agent">
            <xsl:apply-templates select="RoleIDCode"/>
            <xsl:if test="@UserID">
                <xsl:element namespace="http://hl7.org/fhir" name="f:userId">
                    <xsl:call-template name="IdentifierValue">
                        <xsl:with-param name="identifierValue" select="@UserID"/>
                    </xsl:call-template>
                </xsl:element>
            </xsl:if>
            <xsl:if test="@AlternativeUserId">
                <xsl:element namespace="http://hl7.org/fhir" name="f:altId">
                    <xsl:call-template name="String">
                        <xsl:with-param name="value" select="@AlternativeUserId"/>
                    </xsl:call-template>
                </xsl:element>
            </xsl:if>
            <xsl:if test="@UserName">
                <xsl:element namespace="http://hl7.org/fhir" name="f:name">
                    <xsl:call-template name="String">
                        <xsl:with-param name="value" select="@UserName"/>
                    </xsl:call-template>
                </xsl:element>
            </xsl:if>

            <xsl:element namespace="http://hl7.org/fhir" name="f:requestor">
                <xsl:choose>
                    <xsl:when test="@UserIsRequestor">
                        <xsl:call-template name="Boolean">
                            <xsl:with-param name="value" select="@UserIsRequestor"/>
                        </xsl:call-template>
                    </xsl:when>
                    <xsl:otherwise>
                        <!-- requestor element is mandatory in AuditEvent/agent but not in IHE ATNA audit message, force value to false when unknown -->
                        <xsl:call-template name="Boolean">
                            <xsl:with-param name="value">false</xsl:with-param>
                        </xsl:call-template>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:element>
            <xsl:if test="MediaIdentifier/MediaType">
                <xsl:element namespace="http://hl7.org/fhir" name="f:media">
                    <xsl:call-template name="Coding">
                        <xsl:with-param name="codeSystemName" select="MediaIdentifier/MediaType/@codeSystemName"/>
                        <xsl:with-param name="originalText" select="MediaIdentifier/MediaType/@originalText"/>
                        <xsl:with-param name="csdcode" select="MediaIdentifier/MediaType/@csd-code"/>
                    </xsl:call-template>
                </xsl:element>
            </xsl:if>
            <xsl:if test="@NetworkAccessPointID or @NetworkAccessPointTypeCode">
                <xsl:element namespace="http://hl7.org/fhir" name="f:network">
                    <xsl:if test="@NetworkAccessPointID">
                        <xsl:element namespace="http://hl7.org/fhir" name="f:address">
                            <xsl:call-template name="String">
                                <xsl:with-param name="value" select="@NetworkAccessPointID"/>
                            </xsl:call-template>
                        </xsl:element>
                    </xsl:if>
                    <xsl:if test="@NetworkAccessPointTypeCode">
                        <xsl:element namespace="http://hl7.org/fhir" name="f:type">
                            <xsl:call-template name="Code">
                                <xsl:with-param name="value" select="@NetworkAccessPointTypeCode"/>
                            </xsl:call-template>
                        </xsl:element>
                    </xsl:if>
                </xsl:element>
            </xsl:if>
        </xsl:element>
    </xsl:template>

    <xsl:template match="RoleIDCode">
        <xsl:element namespace="http://hl7.org/fhir" name="f:role">
            <xsl:call-template name="CodeableConcept">
                <xsl:with-param name="csdcode" select="@csd-code"/>
                <xsl:with-param name="originalText" select="@originalText"/>
                <xsl:with-param name="codeSystemName" select="@codeSystemName"/>
            </xsl:call-template>
        </xsl:element>
    </xsl:template>

    <xsl:template match="AuditSourceIdentification">
        <xsl:element namespace="http://hl7.org/fhir" name="f:source">
            <xsl:if test="@AuditEnterpriseSiteID">
                <xsl:element namespace="http://hl7.org/fhir" name="f:site">
                    <xsl:call-template name="String">
                        <xsl:with-param name="value" select="@AuditEnterpriseSiteID"/>
                    </xsl:call-template>
                </xsl:element>
            </xsl:if>
            <xsl:if test="@AuditSourceID">
                <xsl:element namespace="http://hl7.org/fhir" name="f:identifier">
                    <xsl:call-template name="IdentifierValue">
                        <xsl:with-param name="identifierValue" select="@AuditSourceID"/>
                    </xsl:call-template>
                </xsl:element>
            </xsl:if>
            <xsl:apply-templates select="AuditSourceTypeCode"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="AuditSourceTypeCode">
        <xsl:element namespace="http://hl7.org/fhir" name="f:type">
            <xsl:call-template name="Coding">
                <xsl:with-param name="codeSystemName" select="@codeSystemName"/>
                <xsl:with-param name="csdcode" select="@csd-code"/>
                <xsl:with-param name="originalText" select="@originalText"/>
            </xsl:call-template>
        </xsl:element>
    </xsl:template>

    <xsl:template match="ParticipantObjectIdentification">
        <xsl:element namespace="http://hl7.org/fhir" name="f:entity">
            <xsl:element namespace="http://hl7.org/fhir" name="f:identifier">
                <xsl:call-template name="IdentifierWithType">
                    <xsl:with-param name="identifierValue" select="@ParticipantObjectID"/>
                    <xsl:with-param name="codeSystemName" select="ParticipantObjectIDTypeCode/@codeSystemName"/>
                    <xsl:with-param name="csdcode" select="ParticipantObjectIDTypeCode/@csd-code"/>
                    <xsl:with-param name="originalText" select="ParticipantObjectIDTypeCode/@originalText"/>
                </xsl:call-template>
            </xsl:element>
            <xsl:element namespace="http://hl7.org/fhir" name="f:reference">
                <xsl:element namespace="http://hl7.org/fhir" name="f:identifier">
                    <xsl:call-template name="IdentifierValue">
                        <xsl:with-param name="identifierValue" select="@ParticipantObjectID"/>
                    </xsl:call-template>
                </xsl:element>
            </xsl:element>
            <xsl:element namespace="http://hl7.org/fhir" name="f:type">
                <xsl:call-template name="Coding">
                    <xsl:with-param name="csdcode" select="@ParticipantObjectTypeCode"/>
                    <xsl:with-param name="originalText"></xsl:with-param>
                    <xsl:with-param name="codeSystemName">http://hl7.org/fhir/audit-entity-type</xsl:with-param>
                </xsl:call-template>
            </xsl:element>
            <xsl:element namespace="http://hl7.org/fhir" name="f:role">
                <xsl:call-template name="Coding">
                    <xsl:with-param name="csdcode" select="@ParticipantObjectTypeCodeRole"/>
                    <xsl:with-param name="originalText"></xsl:with-param>
                    <xsl:with-param name="codeSystemName">http://hl7.org/fhir/object-role</xsl:with-param>
                </xsl:call-template>
            </xsl:element>
            <xsl:if test="@ParticipantObjectDataLifeCycle">
                <xsl:element namespace="http://hl7.org/fhir" name="f:lifecycle">
                    <xsl:call-template name="Coding">
                        <xsl:with-param name="originalText"></xsl:with-param>
                        <xsl:with-param name="csdcode" select="@ParticipantObjectDataLifeCycle"/>
                        <xsl:with-param name="codeSystemName">http://hl7.org/fhir/dicom-audit-lifecycle</xsl:with-param>
                    </xsl:call-template>
                </xsl:element>
            </xsl:if>
            <xsl:if test="@ParticipantObjectSensitivity">
                <xsl:element namespace="http://hl7.org/fhir" name="f:securityLabel">
                    <xsl:call-template name="Coding">
                        <xsl:with-param name="csdcode" select="@ParticipantObjectSensitivity"/>
                        <xsl:with-param name="originalText"></xsl:with-param>
                        <xsl:with-param name="codeSystemName"></xsl:with-param>
                    </xsl:call-template>
                </xsl:element>
            </xsl:if>
            <xsl:apply-templates select="ParticipantObjectName"/>
            <xsl:apply-templates select="ParticipantObjectQuery"/>
            <xsl:apply-templates select="ParticipantObjectDetail"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="ParticipantObjectQuery">
        <xsl:element namespace="http://hl7.org/fhir" name="f:query">
            <xsl:call-template name="base64Binary">
                <xsl:with-param name="base64BinaryValue" select="text()"/>
            </xsl:call-template>
        </xsl:element>
    </xsl:template>

    <xsl:template match="ParticipantObjectName">
        <xsl:element namespace="http://hl7.org/fhir" name="f:name">
            <xsl:call-template name="String">
                <xsl:with-param name="value" select="text()"/>
            </xsl:call-template>
        </xsl:element>
    </xsl:template>

    <xsl:template name="base64Binary">
        <xsl:param name="base64BinaryValue"/>
        <xsl:attribute name="value">
            <xsl:value-of select="$base64BinaryValue"/>
        </xsl:attribute>
    </xsl:template>

    <xsl:template match="ParticipantObjectDetail">
        <xsl:element namespace="http://hl7.org/fhir" name="f:detail">
            <xsl:element namespace="http://hl7.org/fhir" name="f:type">
                <xsl:call-template name="String">
                    <xsl:with-param name="value" select="@type"/>
                </xsl:call-template>
            </xsl:element>
            <xsl:element namespace="http://hl7.org/fhir" name="f:value">
                <xsl:call-template name="base64Binary">
                    <xsl:with-param name="base64BinaryValue" select="@value"/>
                </xsl:call-template>
            </xsl:element>
        </xsl:element>
    </xsl:template>

    <xsl:template name="Code">
        <!-- fill out an element of datatype code -->
        <xsl:param name="value"/>
        <xsl:attribute name="value">
            <xsl:value-of select="$value"/>
        </xsl:attribute>
    </xsl:template>

    <xsl:template name="Instant">
        <!-- fill out an element of datatype instant -->
        <xsl:param name="value"/>
        <xsl:attribute name="value">
            <xsl:value-of select="$value"/>
        </xsl:attribute>
    </xsl:template>

    <xsl:template name="String">
        <!-- fill out an element of datatype instant -->
        <xsl:param name="value"/>
        <xsl:attribute name="value">
            <xsl:value-of select="$value"/>
        </xsl:attribute>
    </xsl:template>

    <xsl:template name="IdentifierValue">
        <xsl:param name="identifierValue"/>
        <xsl:element namespace="http://hl7.org/fhir" name="f:value">
            <xsl:attribute name="value">
                <xsl:value-of select="$identifierValue"/>
            </xsl:attribute>
        </xsl:element>
    </xsl:template>

    <xsl:template name="IdentifierWithType">
        <xsl:param name="identifierValue"/>
        <xsl:param name="codeSystemName"/>
        <xsl:param name="csdcode"/>
        <xsl:param name="originalText"/>
        <xsl:element namespace="http://hl7.org/fhir" name="f:type">
            <xsl:call-template name="CodeableConcept">
                <xsl:with-param name="originalText" select="$originalText"/>
                <xsl:with-param name="csdcode" select="$csdcode"/>
                <xsl:with-param name="codeSystemName" select="$codeSystemName"/>
            </xsl:call-template>
        </xsl:element>
        <xsl:call-template name="IdentifierValue">
            <xsl:with-param name="identifierValue" select="$identifierValue"/>
        </xsl:call-template>
    </xsl:template>

    <xsl:template name="Boolean">
        <xsl:param name="value"/>
        <xsl:attribute name="value">
            <xsl:value-of select="$value"/>
        </xsl:attribute>
    </xsl:template>

    <xsl:template name="Coding">
        <!-- convert csdcode into CodingElement-->
        <xsl:param name="codeSystemName"/>
        <xsl:param name="csdcode"/>
        <xsl:param name="originalText"/>
        <xsl:if test="$codeSystemName != ''">
            <xsl:element namespace="http://hl7.org/fhir" name="f:system">
                <xsl:attribute name="value">
                    <xsl:call-template name="systemNameToUri">
                        <xsl:with-param name="codeSystemName" select="$codeSystemName"/>
                    </xsl:call-template>
                </xsl:attribute>
            </xsl:element>
        </xsl:if>
        <xsl:if test="$csdcode != ''">
            <xsl:element namespace="http://hl7.org/fhir" name="f:code">
                <xsl:call-template name="String">
                    <xsl:with-param name="value" select="$csdcode"/>
                </xsl:call-template>
            </xsl:element>
        </xsl:if>
        <xsl:if test="$originalText != ''">
            <xsl:element namespace="http://hl7.org/fhir" name="f:display">
                <xsl:call-template name="String">
                    <xsl:with-param name="value" select="$originalText"/>
                </xsl:call-template>
            </xsl:element>
        </xsl:if>
    </xsl:template>

    <xsl:template name="CodeableConcept">
        <xsl:param name="codeSystemName"/>
        <xsl:param name="csdcode"/>
        <xsl:param name="originalText"/>
        <xsl:element namespace="http://hl7.org/fhir" name="f:coding">
            <xsl:call-template name="Coding">
                <xsl:with-param name="codeSystemName" select="$codeSystemName"/>
                <xsl:with-param name="csdcode" select="$csdcode"/>
                <xsl:with-param name="originalText" select="$originalText"/>
            </xsl:call-template>
        </xsl:element>
        <xsl:element namespace="http://hl7.org/fhir" name="f:text">
            <xsl:call-template name="String">
                <xsl:with-param name="value" select="$originalText"/>
            </xsl:call-template>
        </xsl:element>
    </xsl:template>

    <xsl:template name="systemNameToUri">
        <xsl:param name="codeSystemName"/>
        <xsl:choose>
            <xsl:when test="$codeSystemName = 'DCM'">
                <xsl:text>http://dicom.nema.org/resources/ontology/DCM</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$codeSystemName"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>