/// IMPORT
//////////////
import org.apache.commons.lang.StringUtils;
import com.eviware.soapui.support.XmlHolder
import net.ihe.gazelle.wstester.mockrecord.Message
import net.ihe.gazelle.wstester.mockrecord.MessageRecorder
import static net.ihe.gazelle.wstester.mockrecord.EStandard.*
import java.nio.charset.StandardCharsets



/////////////////////////////////////
/////////////////////////////////////
def simulatedActorKeyword_init = "CH:PATIENT_AUDIT_CONSUMER"
def simulatedActorKeyword_resp = "CH:PATIENT_AUDIT_RECORD_REPOSITORY"
def domainKeyword = "EPR"
def transactionKeyword = "CH:ATC"
def responder_ip = "ATCSimulator"
def standard = OTHER
/////////////////////////////////////
/////////////////////////////////////


def request = mockRequest.getHttpRequest().toString()
def response = mockResponse.responseContent.toString()
def sender_ip = mockRequest.getHttpRequest().getRemoteAddr()
def requestType = mockRequest.getProtocol()
def responseType = "patientAuditRecordSimulator"

request = request.substring(request.indexOf("/"),request.indexOf(" HTTP"))
request = "https://ehealthsuisse.ihe-europe.net" + request
request = request.replaceAll('%26', '&')
request = request.replaceAll('%3D', '=')
request = request.replaceAll('%3A', ':')
request = request.replaceAll('%7C', '|')
request = request.replaceAll('%2F', '/')
request = request.replaceAll('%2B', '+')



/////// CAN BE CHANGED ACCORDING TO THE PROPERTIES USED IN THE RESPONSE
////////////////////////////////////////////////////////////////////////
if(request.contains('application/fhir+json')){
  int countProperties = StringUtils.countMatches(response.toString(), '${')
    if (countProperties == 1) {
      def properties = response.substring(response.indexOf('${'), response.indexOf('}') + 1).toString()
      def propertiesName = properties.substring(properties.lastIndexOf('{') + 1, properties.indexOf('}')).toString()
      def propertyNameNew = requestContext.getProperty(propertiesName).toString()
      response = response.replace(properties, propertyNameNew)
  } else {
      for (int i = 1; i < countProperties; i++) {
          def properties = response.substring(response.indexOf('${'), response.indexOf('}') + 1).toString()
          def propertiesName = properties.substring(properties.lastIndexOf('{') + 1, properties.indexOf('}')).toString()
          if(requestContext.getProperty(propertiesName)==null){
              response = response.replace(properties, "")
          }else{
              def newProperty = requestContext.getProperty(propertiesName).toString()
              def messageProperty = requestContext.getProperty('message').toString()
              response = response.replace('${message}', messageProperty)
              response = response.replace(properties, newProperty)
          }
      }
  }
}else{

int countProperties = StringUtils.countMatches(response.toString(), '${')
  if (countProperties == 1) {

      def properties = response.substring(response.indexOf('${'), response.indexOf('}') + 1).toString()
      def propertiesName = properties.substring(properties.lastIndexOf('{') + 1, properties.indexOf('}')).toString()
      def newPropertyName = requestContext.getProperty(propertiesName).toString()
      response = response.replace(properties, newPropertyName)
  } else {
      for (int i = 1; i <= countProperties; i++) {
          def properties = response.substring(response.indexOf('${'), response.indexOf('}') + 1).toString()
          def propertiesName = properties.substring(properties.lastIndexOf('{') + 1, properties.indexOf('}')).toString()
          if(requestContext.getProperty(propertiesName)==null){
              response = response.replace(properties, "")
          }else{
              def newProperty = requestContext.getProperty(propertiesName).toString()
              response = response.replace(properties, newProperty)
          }
      }
  }
}

byte[] byte_request = request.getBytes(StandardCharsets.UTF_8)
byte[] byte_response = response.getBytes(StandardCharsets.UTF_8)

MessageRecorder messageRecorder = new MessageRecorder("jdbc:postgresql://localhost:5432/gazelle-webservice-tester", "gazelle", "gazelle")
Message requestMessage = new Message(sender_ip, sender_ip, requestType, simulatedActorKeyword_init, byte_request)
Message responseMessage = new Message(responder_ip, responder_ip, responseType, simulatedActorKeyword_resp, byte_response)
messageRecorder.record(standard, transactionKeyword, domainKeyword, simulatedActorKeyword_resp, requestMessage, responseMessage)
